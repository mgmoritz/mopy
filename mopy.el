;;; mopy.el --- Moritz python wrapper

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Marcos G Moritz <marcosmoritz@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((cl-lib) (virtualenvwrapper))
;; Keywords: python, virtualenv, virtualenvwrapper
;; URL: https://mo-profile.com/mopy

;;; Commentary:
;; This package creates a new python project with a virtualenv

;; It has functions to create a new project and a new virtualenv
;; and allow easy use of virtualenvwrapper

(require 'virtualenvwrapper)
(require 'cl-lib)

(defvar mopy-project-dir "~/workspace/studies/python"
  "Default directory for all python projects")

(defvar mopy--venvdir "~/workspace/venvs"
  "Directory to store all virtual envs")

(defun mopy-new-project ()
  (interactive)
  (let ((current-directory mopy-project-dir)
        (project-name (read-string "Project name: ")))
    (make-directory (format "%s/%s" mopy-project-dir project-name) t)
    (mopy-new-venv (format "%s/%s" mopy--venvdir project-name))
    (make-empty-file (format "%s/%s/requirements.txt" mopy-project-dir project-name))
    (find-file (format "%s/%s/requirements.txt" mopy-project-dir project-name))
    (moritz/load-venvs-from-venvdir)
    (venv-workon project-name)
    (message "Created project %s" project-name)))

(defun mopy-install-requirements ()
  (interactive)
  (async-shell-command (format "pip install -r %s/%s/requirements.txt" mopy-project-dir venv-current-name))
  (message "Installing requirements for project %s" venv-current-name))

(defun mopy-new-venv (venv-name)
  (shell-command (format "python3 -m venv %s" venv-name)))

(defun moritz/load-venvs-from-venvdir ()
  "Get list of directories in the directory `mopy--venvdir'
          and store the result in `moritz/venvs'."
  (interactive)
  (eval-when-compile
    (require 'virtualenvwrapper))
  (let ((default-directory mopy--venvdir))
    (if (file-exists-p default-directory)
        (let ((first-directory (mapcar
                                #'expand-file-name
                                (cddr
                                 (mapcar
                                  #'car
                                  (cl-remove-if-not
                                   #'cadr
                                   (directory-files-and-attributes default-directory)))))))
          (if first-directory
              (setq venv-location first-directory))))))

(moritz/load-venvs-from-venvdir)

;; (if (listp venv-location)
;;     (venv-workon (file-relative-name (car venv-location) moritz/venvdir)))

(provide 'mopy)
